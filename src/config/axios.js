import axios from "axios"

const axiosInstance = axios.create({
    baseURL:
        process.env.NEXT_PUBLIC_API_URL ||
        "http://127.0.0.1:1337" ||
        "http://localhost:1337",
    headers: {
        "Content-Type": "application/json",
    },
    withCredentials: true,
})

export default axiosInstance
