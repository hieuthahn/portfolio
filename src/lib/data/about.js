export default {
  name: "Nguyễn Thành Hiếu",
  jobTitle: "Frontend Developer",
  introduction:
    "I am a Frontend Developer focused on building beautiful and high-performance system using cutting-edge technologies.",
  quote:
    "Những người mình gặp là những người nên gặp, những việc xảy ra là những điều nên xảy ra, mọi thứ xuất hiện đều có lí do của nó, dù là chuyện vui buồn, bất kể là gì đi nữa đều ảnh hưởng và khiến bản thân tốt hơn.",
  description: "Hi there ^^",
  skills: {
    tools: ["Github", "GitLab", "Docker"],
    databases: ["MongoDB", "MySQL"],
    languages: ["Typescript", "Javascript"],
    frameworks: ["NextJS", "ReactJS", "React Native"],
  },
  experience: [
    {
      time: "Dec 2023 - Now",
      company: "Cyno Software",
      website: "https://cyno.com.vn",
      position: "Front-end Developer",
    },
    {
      time: "Mar 2022 - Nov 2023",
      company: "CodeTot JSC",
      website: "https://codetot.vn",
      position: "Front-end Developer",
    },
  ],
  projects: [
    {
      name: "Coffee Mine",
      team: 1,
      github: "https://github.com/hieuthahn/CafeApp-Frontend",
      gitlab: "https://githlab.com/hieuthahn/CafeApp-Frontend",
      website: "https://coffee-mine.vercel.app",
      technology: ["NextJS", "NodeJS", "MongoDB"],
      description: "An Website to find coffee place by many purpose, review, sharing place for anyone",
    },
    {
      name: "Movie review",
      team: 1,
      github: "https://github.com/hieuthahn/react-movie-frontend",
      gitlab: "https://gitlab.com/hieuthahn/react-movie-frontend",
      website: "https://react-movie-frontend.vercel.app",
      technology: ["ReactJS", "HTML", "CSS"],
      description: "An Website to find showing movies and actor also review of every movie",
    },
  ],
  createdAt: "2023-03-25T10:16:10.919Z",
  updatedAt: "2023-05-10T15:50:07.653Z",
  publishedAt: "2023-03-25T10:16:12.734Z",
  locale: "en",
  social_networks: [
    {
      icon: "SiGmail",
      link: "mailto:hieuthahn@gmail.com",
    },
    {
      icon: "SiLinkedin",
      link: "https://www.linkedin.com/in/hieuthahn/",
    },
    {
      icon: "SiGithub",
      link: "https://github.com/hieuthahn",
    },
    {
      icon: "SiGitlab",
      link: "https://gitlab.com/hieuthahn",
    },
    {
      icon: "SiFacebook",
      link: "https://www.facebook.com/hieuthahn",
    },
  ],
};
